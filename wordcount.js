function countWords(str){
    str = str.split(' ').reduce((obj, element) => {
        if(obj[element]){
            obj[element]++;
        }else{
            obj[element] = 1;
        }
        return obj;
    }, {});
    console.log(str)
}

let string = "the quick brown fox is quick and brown";
countWords(string);